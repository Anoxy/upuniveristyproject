# README

# System monitorujący liczbę osób w pomieszczeniu

Projekt ma na celu stworzenie systemu monitorującego pokój oraz zliczającego liczbę osób w po-mieszczeniu. W zakresie projektu jest zarówno stworzenie aplikacji która zajmie się detekcją osób naobrazie, jak i utworzenie systemu sprzętowego oraz mini aplikacji która połączy ze sobą obie funkcjonalności.

## Wersja ##
1.0

## Dokumentacja ##

Interkatywna dokumentacja znajduje się pod adresem http://docs.anoxy.usermd.net

## Uruchamianie ##

* Po pobraniu plików z repozytorium należy wywołać z konsoli plik project/index.py używając polecenia python index.py

## Znane problemy ##
Urządzenie po długim używaniu aplikacji się nagrzewa.