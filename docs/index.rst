System monitorujący liczbę osób w pomieszczeniu
=====================================

.. automodule:: project.index
   :members:

.. automodule:: project.YOLO.YOLO
   :members:

.. automodule:: project.YOLO.utils
   :members:   

.. automodule:: project.YOLO.model
   :members:      

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Wskaźniki i tabele
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
