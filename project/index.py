import time
import cv2 
import os
import sys
sys.path.insert(0, os.path.abspath('..'))

from flask import Flask, render_template, Response
from project.YOLO.YOLO import YOLO

app = Flask(__name__)



start = False

@app.route('/')
def index():
  """
    Funkcja renderuje interfejs użytkownika.
    Wciskając przycisk START rozpoczynamy wdziałanie systemu.
    Wciskając przycisk STOP kończymy działanie systemu, kamera pozostaje włączona, jednak nie przesyła żadnego obrazu.
  """
  return render_template('index.html')

@app.route('/generate')
def generate():
  """
    Funkcja generuje aktualną klatkę do wyświetlenia na ekranie i zwraca ją jako obrazek który następnie może być wyświetlony jako src obiektu <img>.
  """
  yolo = YOLO()
  cap = cv2.VideoCapture(0)

  while True:
    ret, frame = cap.read()
    frame = cv2.resize(frame, None, fx=1.0, fy=1.0, interpolation=cv2.INTER_AREA)  
    r_image, ObjectsList = yolo.detect_img(frame)
    end = cv2.imencode('.jpg', r_image)[1].tobytes()
    
    yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + end + b'\r\n')

    key = cv2.waitKey(20)
    if key == 27:
        break  

@app.route('/video')
def video_feed():
  """
    Funkcja tworzy odpowiednią odpowiedź nagłówka http korzystając z klatek dostarczonych z funkcji generate.
  """
  return Response(generate(), mimetype='multipart/x-mixed-replace; boundary=frame')  
  

if __name__ == "__main__": 
  app.run(host='0.0.0.0')