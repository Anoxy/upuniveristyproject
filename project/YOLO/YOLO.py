import colorsys
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '0'
import cv2
import time

import numpy as np
from keras import backend as K
from keras.models import load_model
from keras.layers import Input

from project.YOLO.model import yolo_eval, yolo_body, tiny_yolo_body
from project.YOLO.utils import image_preporcess

class YOLO(object):
    _defaults = {
        #"model_path": 'logs/trained_weights_final.h5',
        "model_path": '../project/YOLO/model_data/yolo_weights.h5',
        "anchors_path": '../project/YOLO/model_data/yolo_anchors.txt',
        "classes_path": '../project/YOLO/model_data/coco_classes.txt',
        "score" : 0.5,
        "iou" : 0.45,
        "model_image_size" : (96, 96),
        "text_size" : 1,
    }

    @classmethod
    def get_defaults(cls, n):
        """ 
            Pobranie wartości domyślnych obiektu tj, 
            - ścieżka do modelu,
            - ścieżka do kotwic,
            - klasy obiektów,
            - iou,
            - minimalny wymagany wynik żeby uznać wykrycie za poprawne,
            - rozmiar testowanego kwadratu,
            - rozmiar tekstu
        """
        if n in cls._defaults:
            return cls._defaults[n]
        else:
            return "Unrecognized attribute name '" + n + "'"

    def __init__(self, **kwargs):
        """ Konstruktor klasy YOLO"""
        K.clear_session()
        self.__dict__.update(self._defaults) # set up default values
        self.__dict__.update(kwargs) # and update with user overrides
        self.class_names = self._get_class()
        self.anchors = self._get_anchors()
        self.sess = K.get_session()
        self.boxes, self.scores, self.classes = self.generate()

    def _get_class(self):
        """ Pobieranie klas obiektów, które model jest w stanie zidentyfikować """
        classes_path = os.path.expanduser(self.classes_path)
        with open(classes_path) as f:
            class_names = f.readlines()
        class_names = [c.strip() for c in class_names]
        return class_names

    def _get_anchors(self):
        """ Pobieranie współrzędnych punktów kotwicowych służących do detekcji obiektów """
        anchors_path = os.path.expanduser(self.anchors_path)
        with open(anchors_path) as f:
            anchors = f.readline()
        anchors = [float(x) for x in anchors.split(',')]
        return np.array(anchors).reshape(-1, 2)

    def generate(self):
        """ Funkcja generatora. Służy do załadowania modelu oraz ustawienia odpowiednich parametrów obiektu """
        model_path = os.path.expanduser(self.model_path)
        assert model_path.endswith('.h5'), 'Keras model or weights must be a .h5 file.'

        # Load model, or construct model and load weights.
        num_anchors = len(self.anchors)
        num_classes = len(self.class_names)
        is_tiny_version = num_anchors==6 # default setting
        try:
            self.yolo_model = load_model(model_path, compile=False)
        except:
            self.yolo_model = tiny_yolo_body(Input(shape=(None,None,3)), num_anchors//2, num_classes) \
                if is_tiny_version else yolo_body(Input(shape=(None,None,3)), num_anchors//3, num_classes)
            self.yolo_model.load_weights(self.model_path) # make sure model, anchors and classes match
        else:
            assert self.yolo_model.layers[-1].output_shape[-1] == \
                num_anchors/len(self.yolo_model.output) * (num_classes + 5), \
                'Mismatch between model and given anchor and class sizes'

        print('{} model, anchors, and classes loaded.'.format(model_path))

        # Generate colors for drawing bounding boxes.
        hsv_tuples = [(x / len(self.class_names), 1., 1.)
                      for x in range(len(self.class_names))]
        self.colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
        self.colors = list(
            map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)),
                self.colors))

        np.random.shuffle(self.colors)  # Shuffle colors to decorrelate adjacent classes.

        # Generate output tensor targets for filtered bounding boxes.
        self.input_image_shape = K.placeholder(shape=(2, ))
        boxes, scores, classes = yolo_eval(self.yolo_model.output, self.anchors,
                len(self.class_names), self.input_image_shape,
                score_threshold=self.score, iou_threshold=self.iou)
        return boxes, scores, classes

    def detect_image(self, image):
        """ 
            Funkcja wykrywająca obiekty na obrazie. 
            Funkcja przyjmuje jako argument obrazek, oraz zwraca zmodyfikowany obrazek oraz tablicę wykrytych obiektów.
            Zmodyfikowany obraz zawiera stosowną informację na temat braku, lub liczby osób na ekranie.
        """
        if self.model_image_size != (None, None):
            assert self.model_image_size[0]%32 == 0, 'Multiples of 32 required'
            assert self.model_image_size[1]%32 == 0, 'Multiples of 32 required'
            boxed_image = image_preporcess(np.copy(image), tuple(reversed(self.model_image_size)))
            image_data = boxed_image

        out_boxes, out_scores, out_classes = self.sess.run(
            [self.boxes, self.scores, self.classes],
            feed_dict={
                self.yolo_model.input: image_data,
                self.input_image_shape: [image.shape[0], image.shape[1]],
                K.learning_phase(): 0
            })

        #print('Found {} boxes for {}'.format(len(out_boxes), 'img'))

        thickness = (image.shape[0] + image.shape[1]) // 600
        fontScale=1
        ObjectsList = []
        count = 0

        for i, c in reversed(list(enumerate(out_classes))):
            predicted_class = self.class_names[c]
            if predicted_class == 'person':
                count = count + 1
                box = out_boxes[i]
                score = out_scores[i]

                label = '{}'.format(predicted_class)
                scores = '{:.2f}'.format(score)

                top, left, bottom, right = box
                top = max(0, np.floor(top + 0.5).astype('int32'))
                left = max(0, np.floor(left + 0.5).astype('int32'))
                bottom = min(image.shape[0], np.floor(bottom + 0.5).astype('int32'))
                right = min(image.shape[1], np.floor(right + 0.5).astype('int32'))

                mid_h = (bottom-top)/2+top
                mid_v = (right-left)/2+left

                # put object rectangle
                cv2.rectangle(image, (left, top), (right, bottom), self.colors[c], thickness)

                # add everything to list
                ObjectsList.append([top, left, bottom, right, mid_v, mid_h, label, scores])

        if count == 0:
            cv2.putText(image, "Brak ludzi w pomieszczeniu", (0, 30), cv2.FONT_HERSHEY_SIMPLEX, thickness/self.text_size, (255, 0, 0), 2)
        else:
            cv2.putText(image, "Liczba ludzi w pomieszczeniu: {}".format(count), (0, 30), cv2.FONT_HERSHEY_SIMPLEX, thickness/self.text_size, (255, 0, 0), 2)       
        

        return image, ObjectsList

    def close_session(self):
        """ Funkcja służąca do zamknięcia sesji """
        self.sess.close()

    def detect_img(self, image):
        """ 
            Funkcja śłużąca jako funkcja pomocnicza do wykrywania obiektów na obrazie. 
            Przyjmuje jako argument obrazek, oraz swraca zmodyfikowany obraz oraz listę wykrytych obiektów
        """
        #image = cv2.imread(image, cv2.IMREAD_COLOR)
        original_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        original_image_color = cv2.cvtColor(original_image, cv2.COLOR_BGR2RGB)
        
        r_image, ObjectsList = self.detect_image(original_image_color)
        return r_image, ObjectsList